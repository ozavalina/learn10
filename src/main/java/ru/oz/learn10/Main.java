package ru.oz.learn10;

import org.apache.commons.lang3.StringUtils;

public class Main {
    public static void main(String[] args) {
        String str = "Вышел ёжик из тумана!..";
        System.out.println(StringUtils.reverse(str));
        System.out.println(StringUtils.swapCase(str));
    }
}
